package com.ifc.poo;

public interface Automovel {
    void acelerar();
    void frear();
    void acenderFarol();
}