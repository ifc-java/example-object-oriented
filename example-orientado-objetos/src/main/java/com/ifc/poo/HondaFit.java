package com.ifc.poo;

// "extends" estabelece a relação de herança dom a classe Carro
public class HondaFit extends Carro{

    public HondaFit(String modelo, MecanismoAceleracao mecanismoAceleracao) {
        // chama o construtor da classe mãe, ou seja, da classe "Carro"
        super(modelo, mecanismoAceleracao);
    }
}
