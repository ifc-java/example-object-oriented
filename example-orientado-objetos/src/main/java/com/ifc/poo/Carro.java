package com.ifc.poo;

public class Carro implements Automovel {
    private Double velocidade;
    private String modelo;
    private MecanismoAceleracao mecanismoAceleracao;
    private String cor;

    /* Repare que o mecanismo de aceleração é inserido no carro ao ser construído, e
        não o vemos nem podemos modificá-lo, isto é, não tem getter nem setter.
        Já o "modelo" pode ser visto, mas não alterado. */

    public Carro(String modelo, MecanismoAceleracao mecanismoAceleracao) {
        this.modelo = modelo;
        this.mecanismoAceleracao = mecanismoAceleracao;
        this.velocidade = Double.valueOf(0);
    }

    @Override
    public void acelerar() {
        this.mecanismoAceleracao.acelerar();
        System.out.println("ACELERANDO O CARRO");
    }

    @Override
    public void frear() {
        /* código do carro para frear */
        System.out.println("FREANDO O CARRO");
    }

    @Override
    public void acenderFarol() {
        /* código do carro para acender o farol */
        System.out.println("ACENDENDO FAROL DO CARRO");
    }

    public Double getVelocidade() {
        return this.velocidade;
    }

    private void setVelocidade() {
        /* código para alterar a velocidade do carro */
        /* Como só o próprio carro deve calcular a velocidade,
            esse método não pode ser chamado de fora, por isso é "private" */
    }

    public String getModelo() {
        return this.modelo;
    }

    public String getCor() {
        return this.cor;
    }

    /* podemos mudar a cor do carro quando quisermos */
    public void setCor(String cor) {
        this.cor = cor;
    }
}
