package com.ifc.poo;

public class Moto implements Automovel {

    MecanismoDeAceleracaoDeMotos mecanismoDeAceleracaoDeMotos;
    String modelo;
    public Moto(String modelo, MecanismoDeAceleracaoDeMotos mecanismoDeAceleracaoDeMotos) {
        this.mecanismoDeAceleracaoDeMotos = mecanismoDeAceleracaoDeMotos;
        this.modelo = modelo;
    }

    /* ... */

    @Override
    public void acelerar() {
        /* código específico da moto para acelerar */
        System.out.println("ACELERANDO A MOTO");
    }

    @Override
    public void frear() {
        /* código específico da moto para frear */
        System.out.println("FREANDO A MOTO");
    }

    @Override
    public void acenderFarol() {
        /* código específico da moto para acender o farol */
        System.out.println("ACENDENDO FAROL DA MOTO");
    }

    /* ... */
}
